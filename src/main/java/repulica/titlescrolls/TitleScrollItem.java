package repulica.titlescrolls;


import java.util.List;

import javax.annotation.Nullable;

import top.theillusivec4.curios.api.type.capability.ICurio;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class TitleScrollItem extends Item implements ICurio {
	public TitleScrollItem(Properties properties) {
		super(properties);
	}

	public ITextComponent getTitle(ItemStack stack) {
		return stack.getDisplayName();
	}

	public int getRibbonColor(ItemStack stack) {
		return MathHelper.hsvToRGB(Math.abs(getTitle(stack).getString().hashCode() % 255 / 255f), 1f, 1f);
	}

	@Override
	public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		if (this == TitleScrolls.TITLE_SCROLL && !stack.hasDisplayName()) tooltip.add(new TranslationTextComponent("message.titlescrolls.rename"));
	}
}
