package repulica.titlescrolls.api;

import repulica.titlescrolls.TitleEffectsImpl;

import net.minecraft.util.ResourceLocation;

//todo: different setup for forge?
public interface TitleEffects {
	TitleEffects INSTANCE = new TitleEffectsImpl();

	/**
	 * register a new title effect
	 * @param id the id to register with
	 * @param effect the effect to register
	 */
	void registerEffect(ResourceLocation id, TitleEffect effect);

	/**
	 * get a title effect
	 * @param id the id of the effect to get
	 * @return the effect with that id, or {@link TitleEffect#NONE}
	 */
	TitleEffect getEffect(ResourceLocation id);
}

