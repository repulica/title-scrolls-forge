package repulica.titlescrolls.api;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.item.ItemStack;

public interface TitleEffect {
	/**
	 * a no-op title effect
	 */
	TitleEffect NONE = ((stack, slot, matrixStack, buffer, light, player, headYaw, headPitch) -> {});

	/**
	 * render an effect when a certain title is worn
	 * @param stack the stack of the title scroll
	 * @param slot the slot the title is in
	 * @param matrixStack the matrixstack used to render
	 * @param buffer the render type buffer used to render
	 * @param light the light level to render with
	 * todo: player model possible?
	 * @param player the player
	 * @param headYaw the player's head yaw
	 * @param headPitch the player's head pitch
	 */
	void render(ItemStack stack, String slot, MatrixStack matrixStack, IRenderTypeBuffer buffer, int light,
				AbstractClientPlayerEntity player,
				float headYaw, float headPitch);

}
