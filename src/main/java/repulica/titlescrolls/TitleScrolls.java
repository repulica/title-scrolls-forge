package repulica.titlescrolls;

import net.minecraft.block.Block;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Rarity;
import net.minecraft.util.ResourceLocation;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.AddReloadListenerEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import top.theillusivec4.curios.api.SlotTypeMessage;

//todo: mixins possibly not working
// The value here should match an entry in the META-INF/mods.toml file
@Mod("titlescrolls")
public class TitleScrolls {
	public static final String MODID = "titlescrolls";

	// Directly reference a log4j logger.
	public static final Logger LOGGER = LogManager.getLogger(MODID);

	private static final String PROTOCOL_VERSION = "1";

	public static final SimpleChannel PACKETS = NetworkRegistry.newSimpleChannel(
			new ResourceLocation(TitleScrolls.MODID, "update_packets"),
			() -> PROTOCOL_VERSION,
			PROTOCOL_VERSION::equals,
			PROTOCOL_VERSION::equals
	);

	public static Item TITLE_SCROLL;
	public static Item UNCOMMON_TITLE_SCROLL;
	public static Item RARE_TITLE_SCROLL;
	public static Item EPIC_TITLE_SCROLL;

	public TitleScrolls() {
		// Register the setup method for modloading
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
		// Register the enqueueIMC method for modloading
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::enqueueIMC);
		// Register the doClientStuff method for modloading
//		FMLJavaModLoadingContext.get().getModEventBus().addListener(TitleScrollsClient::init);
//		FMLJavaModLoadingContext.get().getModEventBus().addListener(TitleScrollsClient::onItemColorsInit);
//		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onPlayerJoin);
		// Register ourselves for server and other game events we are interested in
		MinecraftForge.EVENT_BUS.register(this);
	}

	private void setup(final FMLCommonSetupEvent event) {
//		// some preinit code
//		LOGGER.info("HELLO FROM PREINIT");
//		LOGGER.info("DIRT BLOCK >> {}", Blocks.DIRT.getRegistryName());
		PACKETS.registerMessage(0, TitleManager.class, TitleManager::toPacket,
				TitleManager::fromPacket, TitleManager::handle);
	}

	private void enqueueIMC(final InterModEnqueueEvent event) {
		// some example code to dispatch IMC to another mod
//		InterModComms.sendTo("titlescrolls", "helloworld", () -> {
//			LOGGER.info("Hello world from the MDK");
//			return "Hello world";
//		});

		InterModComms.sendTo("curios", SlotTypeMessage.REGISTER_TYPE, () ->
				new SlotTypeMessage.Builder("title")
						//todo: not drawing
						.icon(id("textures/item/title_scroll_slot.png"))
						.build());
	}

	@SubscribeEvent
	public void onPlayerJoin(PlayerEvent.PlayerLoggedInEvent event) {
		if (event.getPlayer() instanceof ServerPlayerEntity) {
			ServerPlayerEntity player = (ServerPlayerEntity)event.getPlayer();
			PACKETS.sendTo(TitleManager.INSTANCE, player.connection.netManager, NetworkDirection.PLAY_TO_CLIENT);
		}
	}

	@SubscribeEvent
	public void onAddReloadListeners(AddReloadListenerEvent event) {
		event.addListener(TitleManager.INSTANCE);
	}

	// You can use EventBusSubscriber to automatically subscribe events on the contained class (this is subscribing to the MOD
	// Event bus for receiving Registry Events)
	@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class RegistryEvents {
		@SubscribeEvent
		public static void onItemRegistry(final RegistryEvent.Register<Item> event) {
			event.getRegistry().registerAll(
					TITLE_SCROLL = new TitleScrollItem(new Item.Properties().maxStackSize(1).group(ItemGroup.MISC)).setRegistryName(id("title_scroll")),
					UNCOMMON_TITLE_SCROLL = new SpecialTitleScrollItem(new Item.Properties().maxStackSize(1).rarity(Rarity.UNCOMMON)).setRegistryName(id("uncommon_title_scroll")),
					RARE_TITLE_SCROLL = new SpecialTitleScrollItem(new Item.Properties().maxStackSize(1).rarity(Rarity.RARE)).setRegistryName(id("rare_title_scroll")),
					EPIC_TITLE_SCROLL = new SpecialTitleScrollItem(new Item.Properties().maxStackSize(1).rarity(Rarity.EPIC)).setRegistryName(id("epic_title_scroll"))

			);
		}
	}

	public static ResourceLocation id(String name) {
		return new ResourceLocation(MODID, name);
	}
}
