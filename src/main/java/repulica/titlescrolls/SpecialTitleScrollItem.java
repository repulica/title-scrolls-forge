package repulica.titlescrolls;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nullable;

import com.mojang.blaze3d.matrix.MatrixStack;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import repulica.titlescrolls.api.TitleEffects;
import top.theillusivec4.curios.api.CuriosApi;

import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class SpecialTitleScrollItem extends TitleScrollItem {
	public SpecialTitleScrollItem(Properties properties) {
		super(properties);
	}

	@Override
	public ITextComponent getTitle(ItemStack stack) {
		if (stack.getOrCreateTag().contains("Title", 8)) {
			ResourceLocation id = new ResourceLocation(stack.getOrCreateTag().getString("Title"));
			return TitleManager.INSTANCE.getTitle(id).getText();
		}
		return super.getTitle(stack);
	}

	@Override
	public int getRibbonColor(ItemStack stack) {
		if (stack.getOrCreateTag().contains("Title", 8)) {
			ResourceLocation id = new ResourceLocation(stack.getOrCreateTag().getString("Title"));
			return TitleManager.INSTANCE.getTitle(id).getColor();
		}
		return super.getRibbonColor(stack);
	}

	@Override
	public void render(String identifier, int index, MatrixStack matrixStack, IRenderTypeBuffer renderTypeBuffer,
					   int light, LivingEntity livingEntity, float limbSwing, float limbSwingAmount, float partialTicks,
					   float ageInTicks, float netHeadYaw, float headPitch) {
		Optional<ImmutableTriple<String, Integer, ItemStack>> curio = CuriosApi.getCuriosHelper().findEquippedCurio(
				stack -> stack.getItem() instanceof TitleScrollItem, livingEntity);
		if (curio.isPresent() && livingEntity instanceof AbstractClientPlayerEntity) {
			ItemStack stack = curio.get().right;
			if (stack.getOrCreateTag().contains("Title", 8)) {
				ResourceLocation id = new ResourceLocation(stack.getOrCreateTag().getString("Title"));
				ResourceLocation effect = TitleManager.INSTANCE.getTitle(id).getEffect();
				TitleEffects.INSTANCE.getEffect(effect).render(stack, identifier, matrixStack, renderTypeBuffer, light,
						(AbstractClientPlayerEntity) livingEntity, netHeadYaw, headPitch);
			}
		}

	}

	@Nullable
	public ResourceLocation getTitleId(ItemStack stack) {
		if (stack.getOrCreateTag().contains("Title", 8)) {
			return new ResourceLocation(stack.getOrCreateTag().getString("Title"));
		}
		return null;
	}

	@Override
	public boolean hasEffect(ItemStack stack) {
		return true;
	}

	@Override
	public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("message.titlescrolls.title", getTitle(stack)));
		if (stack.getOrCreateTag().contains("Title", 8)) {
			ResourceLocation id = new ResourceLocation(stack.getOrCreateTag().getString("Title"));
			tooltip.addAll(TitleManager.INSTANCE.getTitle(id).getLore());
		}

	}
}
