package repulica.titlescrolls.util;

import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableMap;

import net.minecraft.util.text.TextFormatting;

//java bad, proguard bad, fuck the fact that i need to create this
public class ColorKludge {
	private static final Map<TextFormatting, ColorKludge> FORMATTING_TO_COLOR = Stream.of(TextFormatting.values()).filter(TextFormatting::isColor).collect(ImmutableMap.toImmutableMap(Function.identity(), (formatting) -> new ColorKludge(formatting.getColor(), formatting.getFriendlyName())));
	private static final Map<String, ColorKludge> NAME_TO_COLOR = FORMATTING_TO_COLOR.values().stream().collect(ImmutableMap.toImmutableMap((color) -> color.name, Function.identity()));
	private final int rgb;
	@Nullable
	private final String name;

	private ColorKludge(int rgb, String name) {
		this.rgb = rgb;
		this.name = name;
	}

	private ColorKludge(int rgb) {
		this.rgb = rgb;
		this.name = null;
	}

	public int getRgb() {
		return this.rgb;
	}

	public String getName() {
		return this.name != null ? this.name : this.getHex();
	}

	private String getHex() {
		return String.format("#%06X", this.rgb);
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		} else if (other != null && this.getClass() == other.getClass()) {
			ColorKludge color = (ColorKludge)other;
			return this.rgb == color.rgb;
		} else {
			return false;
		}
	}

	public int hashCode() {
		return Objects.hash(this.rgb, this.name);
	}

	public String toString() {
		return this.name != null ? this.name : this.getHex();
	}

	@Nullable
	public static ColorKludge fromFormatting(TextFormatting formatting) {
		return FORMATTING_TO_COLOR.get(formatting);
	}

	public static ColorKludge fromRgb(int rgb) {
		return new ColorKludge(rgb);
	}

	@Nullable
	public static ColorKludge parse(String color) {
		if (color.startsWith("#")) {
			try {
				int i = Integer.parseInt(color.substring(1), 16);
				return fromRgb(i);
			} catch (NumberFormatException numberformatexception) {
				return null;
			}
		} else {
			return NAME_TO_COLOR.get(color);
		}
	}
}
