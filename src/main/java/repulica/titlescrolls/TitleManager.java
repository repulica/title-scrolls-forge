package repulica.titlescrolls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraftforge.fml.network.NetworkEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import repulica.titlescrolls.util.ColorKludge;

import net.minecraft.client.resources.JsonReloadListener;
import net.minecraft.network.PacketBuffer;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;

public class TitleManager extends JsonReloadListener {
	public static final TitleManager INSTANCE = new TitleManager();
	private static final Logger LOGGER = LogManager.getLogger();

	private final Map<ResourceLocation, Title> titles = new HashMap<>();

	private TitleManager() {
		super(new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().setLenient().create(), "titles");
	}

	@Override
	protected void apply(Map<ResourceLocation, JsonElement> loader, IResourceManager manager, IProfiler profiler) {
		titles.clear();
		for (ResourceLocation id : loader.keySet()) {
			JsonObject json = JSONUtils.getJsonObject(loader.get(id), "title");
			ColorKludge color = ColorKludge.parse(JSONUtils.getString(json, "ribbon_color"));
			JsonObject text = JSONUtils.getJsonObject(json, "title_text");
			String effect = JSONUtils.getString(json, "render_effect", "titlescrolls:none");
			List<ITextComponent> lore = new ArrayList<>();
			if (JSONUtils.isJsonArray(json, "scroll_lore")) {
				JsonArray loreArray = JSONUtils.getJsonArray(json, "scroll_lore");
				for (JsonElement entry : loreArray) {
					JsonObject loreText = JSONUtils.getJsonObject(entry, "<scroll_lore entry>");
					lore.add(ITextComponent.Serializer.func_240641_a_(loreText));
				}
			}
			titles.put(id, new Title(ITextComponent.Serializer.func_240641_a_(text), color, new ResourceLocation(effect), lore));
		}
		LOGGER.info("Loaded {} titles", titles.size());
	}

	public void toPacket(PacketBuffer buf) {
		buf.writeVarInt(titles.size());
		for (ResourceLocation id : titles.keySet()) {
			buf.writeResourceLocation(id);
			Title title = titles.get(id);
			buf.writeTextComponent(title.getText());
			buf.writeVarInt(title.getColor());
			buf.writeResourceLocation(title.getEffect());
			buf.writeVarInt(title.getLore().size());
			for (ITextComponent line : title.getLore()) {
				buf.writeTextComponent(line);
			}
		}
	}

	public static TitleManager fromPacket(PacketBuffer buf) {
		INSTANCE.titles.clear();
		int length = buf.readVarInt();
		for (int i = 0; i < length; i++) {
			ResourceLocation id = buf.readResourceLocation();
			ITextComponent text = buf.readTextComponent();
			int color = buf.readVarInt();
			ResourceLocation effect = buf.readResourceLocation();
			List<ITextComponent> lore = new ArrayList<>();
			int loreLength = buf.readVarInt();
			for (int j = 0; j < loreLength; j++) {
				lore.add(buf.readTextComponent());
			}
			INSTANCE.titles.put(id, new Title(text, color, effect, lore));
		}
		return INSTANCE;
	}

	public void handle(Supplier<NetworkEvent.Context> ctx) {
		ctx.get().setPacketHandled(true);
	}

	public Title getTitle(ResourceLocation id) {
		return titles.getOrDefault(id, Title.NONE);
	}

	public static class Title {
		public static final Title NONE = new Title(new StringTextComponent("Blank Scroll"), 0xFFFFFF,
				new ResourceLocation(TitleScrolls.MODID, "none"), new ArrayList<>());

		private final ITextComponent text;
		private final int color;
		private final ResourceLocation effect;
		private final List<ITextComponent> lore;

		public Title(ITextComponent text, @Nullable ColorKludge color, ResourceLocation effect, List<ITextComponent> lore) {
			this(text, color == null? 0xFF0000 : color.getRgb(), effect, lore);
		}

		public Title(ITextComponent text, int color, ResourceLocation effect, List<ITextComponent> lore) {
			this.text = text;
			this.color = color;
			this.effect = effect;
			this.lore = lore;
		}

		public ITextComponent getText() {
			return text;
		}

		public int getColor() {
			return color;
		}

		public ResourceLocation getEffect() {
			return effect;
		}

		public List<ITextComponent> getLore() {
			return lore;
		}
	}

}
