package repulica.titlescrolls.mixin;

import java.util.Optional;

import com.mojang.blaze3d.matrix.MatrixStack;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import repulica.titlescrolls.TitleScrollItem;
import top.theillusivec4.curios.api.CuriosApi;

import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;

@Mixin(PlayerRenderer.class)
public abstract class MixinPlayerRenderer extends LivingRenderer<AbstractClientPlayerEntity,
		PlayerModel<AbstractClientPlayerEntity>> {

	public MixinPlayerRenderer(EntityRendererManager rendererManager,
							   PlayerModel<AbstractClientPlayerEntity> entityModelIn, float shadowSizeIn) {
		super(rendererManager, entityModelIn, shadowSizeIn);
	}

	//TODO: handle own rendering?

	@Inject(method = "renderName", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/entity/player/AbstractClientPlayerEntity;getWorldScoreboard()Lnet/minecraft/scoreboard/Scoreboard;"),
			cancellable = true)
	private void injectTitleRender(AbstractClientPlayerEntity player, ITextComponent text,
								   MatrixStack matrices, IRenderTypeBuffer vertices, int light,
								   CallbackInfo info) {
		Optional<ImmutableTriple<String, Integer, ItemStack>> curio = CuriosApi.getCuriosHelper().findEquippedCurio(
				stack -> stack.getItem() instanceof TitleScrollItem, player);
//		ItemStack stack = TrinketsApi.getTrinketComponent(player).getStack("misc", "title");
		if ((curio.isPresent())) {
			ItemStack stack = curio.get().right;
			ITextComponent title = ((TitleScrollItem) stack.getItem()).getTitle(stack);
			matrices.push();
			matrices.scale(0.75f, 0.75f, 0.75f);
			matrices.translate(0f, 0.6f, 0f);
			super.renderName(player, title, matrices, vertices, light);
			matrices.pop();
			matrices.translate(0d, 0.1225d, 0d);
//			matrices.translate(0.0d, (9.0f * 1.15f * 0.025f), 0.0d);
			super.renderName(player, text, matrices, vertices, light);
			matrices.pop();
			info.cancel();
		}
	}

}
