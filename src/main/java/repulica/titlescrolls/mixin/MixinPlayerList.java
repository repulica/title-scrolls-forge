package repulica.titlescrolls.mixin;

import java.util.Iterator;

import net.minecraftforge.fml.network.NetworkDirection;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import repulica.titlescrolls.TitleManager;
import repulica.titlescrolls.TitleScrolls;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.play.server.SUpdateRecipesPacket;
import net.minecraft.server.management.PlayerList;

@Mixin(PlayerList.class)
public class MixinPlayerList {

	//TODO: possible without mixin?
	@Inject(method = "reloadResources", at = @At(value = "INVOKE", target = "Lnet/minecraft/item/crafting/ServerRecipeBook;init(Lnet/minecraft/entity/player/ServerPlayerEntity;)V"),
	locals = LocalCapture.CAPTURE_FAILEXCEPTION)
	private void injectPacketSend(CallbackInfo info, SUpdateRecipesPacket packet, Iterator<ServerPlayerEntity> players,
								  ServerPlayerEntity player) {
		TitleScrolls.PACKETS.sendTo(TitleManager.INSTANCE, player.connection.netManager, NetworkDirection.PLAY_TO_CLIENT);
	}
}
