package repulica.titlescrolls;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import repulica.titlescrolls.api.TitleEffect;
import repulica.titlescrolls.api.TitleEffects;

import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ResourceLocation;

@Mod.EventBusSubscriber(value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
public class TitleScrollsClient {

	//todo: not working, which is really annoying
	@SubscribeEvent
	public static void onItemColorsInit(ColorHandlerEvent.Item event) {
		event.getItemColors().register((stack, tintIndex) -> {
			if (tintIndex == 1) return ((TitleScrollItem) stack.getItem()).getRibbonColor(stack);
			return 0xFFFFFF;

		}, TitleScrolls.TITLE_SCROLL, TitleScrolls.UNCOMMON_TITLE_SCROLL, TitleScrolls.RARE_TITLE_SCROLL, TitleScrolls.EPIC_TITLE_SCROLL);
	}

	@SubscribeEvent
	public static void init(final FMLClientSetupEvent event) {
		TitleEffects.INSTANCE.registerEffect(new ResourceLocation(TitleScrolls.MODID, "none"), TitleEffect.NONE);
		TitleEffects.INSTANCE.registerEffect(new ResourceLocation(TitleScrolls.MODID, "ender"),
				((stack, slot, matrixStack, vertexConsumer, light, player, headYaw, headPitch) ->
						player.world.addParticle(ParticleTypes.PORTAL, player.getPosXRandom(0.5D),
								player.getPosY() - 0.25D, player.getPosZRandom(0.5D),
								(player.getRNG().nextDouble() - 0.5D) * 2.0D,
								-player.getRNG().nextDouble(),
								(player.getRNG().nextDouble() - 0.5D) * 2.0D)
				)
		);
		TitleEffects.INSTANCE.registerEffect(new ResourceLocation(TitleScrolls.MODID, "ashen"),
				((stack, slot, matrixStack, vertexConsumer, light, player, headYaw, headPitch) ->
						player.world.addParticle(ParticleTypes.ASH, player.getPosXRandom(0.5D),
								player.getPosYRandom() - 0.25D, player.getPosZRandom(0.5D),
								(player.getRNG().nextDouble() - 0.5D) * 2.0D,
								-player.getRNG().nextDouble(),
								(player.getRNG().nextDouble() - 0.5D) * 2.0D)
				)
		);

	}
}
