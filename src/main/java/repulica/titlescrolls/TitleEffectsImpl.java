package repulica.titlescrolls;

import java.util.HashMap;
import java.util.Map;

import repulica.titlescrolls.api.TitleEffect;
import repulica.titlescrolls.api.TitleEffects;

import net.minecraft.util.ResourceLocation;

public class TitleEffectsImpl implements TitleEffects {
	private final Map<ResourceLocation, TitleEffect> effects = new HashMap<>();

	@Override
	public void registerEffect(ResourceLocation id, TitleEffect effect) {
		effects.put(id, effect);
	}

	@Override
	public TitleEffect getEffect(ResourceLocation id) {
		return effects.getOrDefault(id, TitleEffect.NONE);
	}

}
